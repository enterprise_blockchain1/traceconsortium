function clearCard() {
    $('#getProductName').text("");
    $('#getProductId').text("");
    $('#getProductOrigin').text("");
    $('#getHarvestDate').text("");
    $('#getExpiryDate').text("");
    $('#getCurrentLocation').text("");
    $('#getProductStatus').text("");
    $('#message').text("");
    $('#editProductForm')[0].reset();
    $('#editProductForm').addClass('d-none');
}

function formattedDate(rfc3339Date) {
    var date = new Date(rfc3339Date);
    var day = date.getDate(); // day of the month
    var month = date.getMonth() + 1; // month (getMonth() returns 0-11, so add 1)
    month = month < 10 ? "0" + month : month;
    day = day < 10 ? "0" + day : day;
    var year = date.getFullYear(); // year
    // format date as YYYY-MM-DD
    var formattedDate = year + '-' + month + '-' + day;
    return formattedDate;
}

// Function to call when 'No' is clicked
function noFunction() {
    $('#confirmModal').modal('hide');
}

// Add Product record to ledger
function createProductRecord() {
    // Get form data
    var productId = $('#productId').val();
    var productName = $('#productName').val();
    var origin = $('#productOrigin').val();
    var harvestDate = new Date($('#harvestDate').val()).toISOString();
    var expiryDate = new Date($('#expiryDate').val()).toISOString();
    var currentLocation = $('#currentLocation').val();
    var status = $('#productStatus').val();
    var data = {
        id: productId,
        name: productName,
        origin: origin,
        harvestDate: harvestDate,
        expiryDate: expiryDate,
        currentLocation: currentLocation,
        status: status
    };

    // Send a POST request to the server
    $.ajax({
        url: '/products',
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (result) {
            // Show the modal
            var myModal = new bootstrap.Modal($('#successModal'), {});
            myModal.show();
        },
        error: function (error) {
            console.log(error);
        }
    }).always(function () {
        // Clear the form
        $('form')[0].reset();
        $('#confirmModal').modal('hide');
        $('#yesButton').off("click");
    });
}

// Remove a Product record from ledger
function deleteProductRecord() {
    var productId = $('#editProductId').text(); // Get the value from the element
    $.ajax({
        url: '/products/' + productId, // append the productId to your API endpoint
        type: 'DELETE',
        success: function (result) {
            // Show the modal
            var myModal = new bootstrap.Modal($('#successModal'), {});
            myModal.show();
            console.log(result);
        },
        error: function (error) {
            console.log(error);
        }
    }).always(function () {
        // Clear the card
        clearCard();
        $('#confirmModal').modal('hide');
        $('#yesButton').off("click");
    });
}

// Edit a Product record in the ledger
function updateProductRecord() {
    var productId = $('#editProductId').text();
    var productName = $('#editProductName').val();
    var origin = $('#editProductOrigin').val();
    var harvestDate = new Date($('#editHarvestDate').val()).toISOString();
    var expiryDate = new Date($('#editExpiryDate').val()).toISOString();
    var currentLocation = $('#editCurrentLocation').val();
    var status = $('#editProductStatus').val();
    var data = {
        name: productName,
        origin: origin,
        harvestDate: harvestDate,
        expiryDate: expiryDate,
        currentLocation: currentLocation,
        status: status
    };

    $.ajax({
        url: '/products/' + productId, // append the productId to your API endpoint
        type: 'PUT',
        data: JSON.stringify(data),
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function (result) {
            var myModal = new bootstrap.Modal($('#successModal'), {});
            myModal.show();
            clearCard();
            console.log(result);
        },
        error: function (error) {
            console.log(error);
        }
    }).always(function () {
        clearCard();
        $('#confirmModal').modal('hide');
        $('#yesButton').off("click");
    });
}

$(document).ready(function () {
    //Add event Handlers
    // Event listener for when the modal is hidden
    $('#confirmModal').on('hidden.bs.modal', function () {
        console.log(result); // Outputs: true if 'Yes' was clicked, false if 'No' was clicked
    });

    // Attach event listeners to the buttons
    $('#noButton').click(noFunction);

    // Handle form submission
    $('#createProductForm').on('submit', function (e) {
        e.preventDefault();
        $('#yesButton').off("click");
        $('#yesButton').click(createProductRecord);
        $('#confirmModal').modal('show');
    });

    $('#getProductForm').on('submit', function (e) {
        e.preventDefault();
        var searchId = $('#searchId').val();
        $.ajax({
            url: '/products/' + searchId,
            type: 'GET',
            success: function (data) {
                data = JSON.parse(data); // Parse into an object
                $('#editProductId').text(data.id);
                $('#editProductName').val(data.name);
                $('#editProductOrigin').val(data.origin);
                $('#editHarvestDate').val(formattedDate(data.harvestDate));
                $('#editExpiryDate').val(formattedDate(data.expiryDate));
                $('#editCurrentLocation').val(data.currentLocation);
                $('#editProductStatus').val(data.status);
                $('#editProductForm').removeClass('d-none');
            },
            error: function (error) {
                clearCard();
                $('#message').text("Not Found");
                console.log(error);
            }
        });
    });

    $('#deleteButton').click(function () {
        $('#yesButton').off("click");
        $('#yesButton').click(deleteProductRecord);
        $('#confirmModal').modal('show');
    });

    $('#updateButton').click(function () {
        $('#yesButton').off("click");
        $('#yesButton').click(updateProductRecord);
        $('#confirmModal').modal('show');
    });
});
