<!-- to set enviroment  -->

PATH="/workspaces/traceconsortium/bin:$PATH"



export GOFLAGS="-buildvcs=false"

<!-- # to generate crypto material  -->
cryptogen generate --config=./crypto-config.yaml --output=crypto-config

<!-- # to generate orderer channel  -->
configtxgen -outputBlock ./orderer/tracegenesis.block -channelID ordererchannel -profile TraceOrdererGenesis
<!-- # to generate tracechannel transaction file -->
configtxgen -outputCreateChannelTx ./tracechannel/tracechannel.tx -channelID tracechannel -profile TraceChannel
<!-- # validating peer -->
chmod +x set_peer_env.sh
. tool-bins/set_peer_env.sh producer
peer channel list
<!-- # to create channel -->
peer channel create -c tracechannel -f ./config/tracechannel/tracechannel.tx --outputBlock ./config/tracechannel/tracechannel.block -o $ORDERER_ADDRESS

<!-- # to join channel  -->
peer channel join -b ./config/tracechannel/tracechannel.block -o $ORDERER_ADDRESS

go mod init productmanagement

chmod +x set_producer_env.sh

. tool-bins/set_producer_env.sh

<!-- to package and install chaincode  -->

peer lifecycle chaincode package $CC_PACKAGE_FILE -p $CC_PATH --label $CC_LABEL

peer lifecycle chaincode install $CC_PACKAGE_FILE

peer lifecycle chaincode queryinstalled

CC_PACKAGE_ID=

peer lifecycle chaincode approveformyorg -n productmgt -v 1.0 -C tracechannel --sequence 1 --package-id $CC_PACKAGE_ID

peer lifecycle chaincode checkcommitreadiness -n productmgt -v 1.0 -C tracechannel --sequence 1

peer lifecycle chaincode commit -n productmgt -v 1.0 -C tracechannel --sequence 1

peer lifecycle chaincode querycommitted -n productmgt -C tracechannel

peer chaincode invoke -C tracechannel -n productmgt -c '{"function":"CreateProduct","Args":["1", "Apple", "USA", "Warehouse A", "Harvested", "2024-05-01T00:00:00Z", "2024-06-01T00:00:00Z"]}'

peer chaincode query -C tracechannel -n productmgt -c '{"function":"ReadProduct","Args":["1"]}'


peer chaincode invoke -C tracechannel -n productmgt -c '{"function":"UpdateProduct","Args":["prod1", "Orange", "Spain", "Warehouse B", "In Transit", "2024-04-01T00:00:00Z", "2024-07-01T00:00:00Z"]}'


<!-- # for chaincode 2 -->
peer lifecycle chaincode approveformyorg -n productmgt -v 1.1 -C tracechannel --sequence 2 --package-id $CC_PACKAGE_ID

peer lifecycle chaincode commit -n productmgt -v 1.1 -C tracechannel --sequence 2

<!-- # to create new product for 2nd chaincode -->
peer chaincode invoke -C tracechannel -n productmgt -c '{"function":"CreateProduct","Args":["1", "Grapes", "Paro", "Warehouse X", "Harvested", "2024-03-01T00:00:00Z", "2024-06-01T00:00:00Z"]}'

<!-- for backend -->

node wallet.js

node wallet.js add producer Admin


node wallet.js list

touch connection.json
code connection.json