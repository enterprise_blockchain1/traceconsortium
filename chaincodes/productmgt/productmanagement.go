package main

import (
"encoding/json"
"fmt"
"time"
"github.com/hyperledger/fabric-contract-api-go/contractapi"
)


// Product represents the information of a product item in the supply chain
type Product struct {
	ID             string    `json:"id"`
	Name           string   `json:"name"`
	Origin         string    `json:"origin"`
	HarvestDate    time.Time `json:"harvestDate"`
	ExpiryDate     time.Time `json:"expiryDate"`
	CurrentLocation string    `json:"currentLocation"`
	Status         string    `json:"status"` // e.g., "Harvested", "In Transit", "Delivered", "Stored"
}

// RecordCount keeps track of the number of product records
type RecordCount struct {
	Count int `json:"count"`
}

// ProductContract provides functions for managing the product supply chain
type ProductContract struct {
	contractapi.Contract
}

// CreateProduct adds a new product item to the blockchain
func (pc *ProductContract) CreateProduct(ctx contractapi.TransactionContextInterface, id, name, origin, currentLocation, status string, harvestDate, expiryDate time.Time) error {
	existingProductJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return err
	}
	if existingProductJSON != nil {
		return fmt.Errorf("a product record with the id '%s' already exists", id)
	}

	product := Product{
		ID:             id,
		Name:           name,
		Origin:         origin,
		HarvestDate:    harvestDate,
		ExpiryDate:     expiryDate,
		CurrentLocation: currentLocation,
		Status:         status,
	}
	productJSON, err := json.Marshal(product)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(id, productJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state: %v", err)
	}

	eventPayload := fmt.Sprintf("Created product: %s", id)
	err = ctx.GetStub().SetEvent("CreateProduct", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register: %v", err)
	}

	return pc.UpdateRecordCount(ctx, 1)
}

// ReadProduct retrieves a product item from the blockchain
func (pc *ProductContract) ReadProduct(ctx contractapi.TransactionContextInterface, id string) (*Product, error) {
	productJSON, err := ctx.GetStub().GetState(id)
	if err != nil {
		return nil, fmt.Errorf("failed to read from world state: %v", err)
	}
	if productJSON == nil {
		return nil, fmt.Errorf("the product %s does not exist", id)
	}

	var product Product
	err = json.Unmarshal(productJSON, &product)
	if err != nil {
		return nil, err
	}

	eventPayload := fmt.Sprintf("Read product: %s", id)
	err = ctx.GetStub().SetEvent("ReadProduct", []byte(eventPayload))
	if err != nil {
		return nil, fmt.Errorf("event failed to register: %v", err)
	}

	return &product, nil
}

// UpdateProduct modifies an existing product item on the blockchain
func (pc *ProductContract) UpdateProduct(ctx contractapi.TransactionContextInterface, id, name, origin, currentLocation, status string, harvestDate, expiryDate time.Time) error {
	product, err := pc.ReadProduct(ctx, id)
	if err != nil {
		return err
	}

	product.Name = name
	product.Origin = origin
	product.CurrentLocation = currentLocation
	product.Status = status
	product.HarvestDate = harvestDate
	product.ExpiryDate = expiryDate

	productJSON, err := json.Marshal(product)
	if err != nil {
		return err
	}

	err = ctx.GetStub().PutState(id, productJSON)
	if err != nil {
		return fmt.Errorf("failed to put to world state: %v", err)
	}

	eventPayload := fmt.Sprintf("Updated product: %s", id)
	err = ctx.GetStub().SetEvent("UpdateProduct", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register: %v", err)
	}

	return nil
}

// DeleteProduct removes a product item from the blockchain
func (pc *ProductContract) DeleteProduct(ctx contractapi.TransactionContextInterface, id string) error {
	err := ctx.GetStub().DelState(id)
	if err != nil {
		return fmt.Errorf("failed to delete state: %v", err)
	}

	eventPayload := fmt.Sprintf("Deleted product: %s", id)
	err = ctx.GetStub().SetEvent("DeleteProduct", []byte(eventPayload))
	if err != nil {
		return fmt.Errorf("event failed to register: %v", err)
	}

	return pc.UpdateRecordCount(ctx, -1)
}

// UpdateRecordCount updates the total number of product records
func (pc *ProductContract) UpdateRecordCount(ctx contractapi.TransactionContextInterface, increment int) error {
	countJSON, err := ctx.GetStub().GetState("recordCount")
	if err != nil {
		return err
	}

	var recordCount RecordCount
	if countJSON == nil {
		recordCount = RecordCount{Count: 0}
	} else {
		err = json.Unmarshal(countJSON, &recordCount)
		if err != nil {
			return err
		}
	}

	recordCount.Count += increment
	newCountJSON, err := json.Marshal(recordCount)
	if err != nil {
		return err
	}

	return ctx.GetStub().PutState("recordCount", newCountJSON)
}

// GetRecordCount returns the total number of product records
func (pc *ProductContract) GetRecordCount(ctx contractapi.TransactionContextInterface) (int, error) {
	countJSON, err := ctx.GetStub().GetState("recordCount")
	if err != nil {
		return 0, err
	}

	var recordCount RecordCount
	err = json.Unmarshal(countJSON, &recordCount)
	if err != nil {
		return 0, err
	}

	return recordCount.Count, nil
}

func main() {
	chaincode, err := contractapi.NewChaincode(new(ProductContract))
	if err != nil {
		fmt.Printf("Error creating ProductTrace chaincode: %s", err.Error())
		return
	}
	if err := chaincode.Start(); err != nil {
		fmt.Printf("Error starting ProductTrace chaincode: %s", err.Error())
	}
}
